#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""PIP setup script for the SDP Processing Controller package."""

import setuptools

with open("version.txt", "r") as fh:
    VERSION = fh.read().rstrip()
with open("README.md", "r") as fh:
    LONG_DESCRIPTION = fh.read()

setuptools.setup(
    name="test-bumping-versions",
    version=VERSION,
    description="Test bumping versions",
    author="Gabriella Hodosan",
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/gabicca/test-bumping-versions",
    package_dir={"": "src"},
    packages=setuptools.find_packages("src"),
    package_data={"test-bumping-versions": ["schema/*.json"]},
    install_requires=[
        "jsonschema",
        "requests",
    ],
    setup_requires=["pytest-runner"],
    tests_require=["pytest"],
)
